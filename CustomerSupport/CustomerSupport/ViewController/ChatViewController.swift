//
//  ChatViewController.swift
//  CustomerSupport
//
//  Created by mufaza on 13/06/2021.
//

import Foundation
import UIKit
import Photos
import MessageKit
import UserNotifications




class ChatViewController: MessagesViewController,UNUserNotificationCenterDelegate {
    
    
  

   var messages: [Message] = []
  
    let channel :Channel
    var data  = [String:Any]()
   
    var list  = [String:Any]()
    
    weak var timer: Timer?
    
    var timeLimit : Int = 10
  


  init(channel: Channel) {
    self.channel = channel
    super.init(nibName: nil, bundle: nil)

    title = channel.creator
  }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
//    deinit {
//        timer?.invalidate()
//        print("Remove NotificationCenter Deinit")
//        NotificationCenter.default.removeObserver(self)
//    }
//
  
   
  override func viewDidLoad() {
    super.viewDidLoad()
    
      self.resetTimer()
      NotificationCenter.default.addObserver(self, selector: #selector(checkReset), name:  Notification.Name("activityNotify"), object: nil)
  }
    
    override func viewWillAppear(_ animated: Bool) {
        NetworkHelper.sharedInstance.shoulUpdateChat = true
        navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationItem.hidesBackButton  = false
        
        maintainPositionOnKeyboardFrameChanged = true
        messageInputBar.inputTextView.tintColor = .primary
        messageInputBar.sendButton.setTitleColor(.primary, for: .normal)
        
        messageInputBar.delegate = self
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        
        self.getChat()
    }
    
    @objc func checkReset(notification : NSNotification){
        if self.timeLimit >= 240 {
            self.getUserChat()
            self.resetTimer()
        }
        self.timeLimit = 10
    }
  
  override func viewDidDisappear(_ animated: Bool) {
    NetworkHelper.sharedInstance.shoulUpdateChat = false
      NotificationCenter.default.removeObserver(self)
    timer?.invalidate()
  }
  
  // MARK: - Actions
  
//  @objc private func cameraButtonPressed() {
//    let picker = UIImagePickerController()
//    picker.delegate = self
//
//    if UIImagePickerController.isSourceTypeAvailable(.camera) {
//      picker.sourceType = .camera
//    } else {
//      picker.sourceType = .photoLibrary
//    }
//
//    present(picker, animated: true, completion: nil)
//  }

    func resetTimer() {
       
        timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(refreshChat), userInfo: nil, repeats: true)
    }

    @objc func refreshChat() {
        if timeLimit < 240 {
           self.getUserChat()
            timeLimit += 10
            debugPrint("mytest : 10 Sec Added :", self.timeLimit)
        } else if timeLimit >= 240 {
            timer?.invalidate()
        }
    }
    
    
  // MARK: - API
    
    func getUserChat()
    {
        var dict : Dictionary <String, String> = [:]
        dict  = ["firebase_id":channel.id ]
        
        let data :Dictionary<String,Any> = ["data": dict as Any]
        print(data)
        
        NetworkHelper.sharedInstance.postChatRequest(serviceName:ApiUrl.GET_USER_MSG, sendData: data as [String : AnyObject], success: { (data) in
           // print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            let status = data["status"] as! Bool

            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async { [self] in
                    if status{
                        messages.removeAll()
                        self.data = data["data"]!["chat"] as! [String : Any]
                        self.parseData(data: self.data)
                    }else{
                        print("Error,try again later")
                    }
                }
                
            }
            
        }, failure: { (data) in
            print(data)
            
        })
        
    }
    
    
    func getChat(){
        var dict : Dictionary <String, String> = [:]
        dict  = ["reciever_id": ConstantStrings.agentID,"app_id":ConstantStrings.appID ,"creator_namespace":channel.creator ,"firebase_id":channel.id ]
        
        let data :Dictionary<String,Any> = ["data": dict as Any]
        print(data)
        
        NetworkHelper.sharedInstance.postChatRequest(serviceName:ApiUrl.CONTINUE_CHAT, sendData: data as [String : AnyObject], success: { (data) in
           // print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            let status = data["status"] as! Bool
            
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async { [self] in
                    // self.showAlert("Success")
                    if status{
                        self.data = data["data"]!["user_chat"] as! [String : Any]
                        var chatData = [String:Any]()
                        chatData = self.data["chat_history"] as!  [String : Any]
                        self.parseData(data: chatData)
                    }else{
                        print("Error,try again later")
                    }
                }
                
            }
            
        }, failure: { (data) in
            print(data)
            
        })
        
    }
    
    
    func save(_ message: Message) {
     print(self.channel.id)
     
     var dict : Dictionary <String, String> = [:]
     dict  = ["sender_id": ConstantStrings.agentID as String, "firebase_id" :channel.id,"sender_msg":message.content,"sender_name":ConstantStrings.agentName,"send_by":"agent"]

     let data :Dictionary<String,Any> = ["data": dict as Any]
     print(data)
     
        NetworkHelper.sharedInstance.postChatRequest(serviceName:ApiUrl.PUSH_AGENT_MSG, sendData: data as [String : AnyObject], success: { (data) in
         print(data)
         let defaults = UserDefaults.standard
         let statusCode = defaults.string(forKey: "statusCode")
         let status = data["status"] as! Bool
            
         if ((statusCode?.range(of: "200")) != nil){
             DispatchQueue.main.async { [self] in
                 // self.showAlert("Success")
                 let status = data["status"] as! Bool
                 if status{
                     print("message saved successfully")
                     insertNewMessage(message)
                 }else{
                    print("Error,try again later")
                 }
             }
             
         }
         
     }, failure: { (data) in
         print(data)
         
     })
   }
    
    // MARK: - Helpers
    func parseData(data: [String:Any])
    {
        var senderID:String = ""
        var content :String = ""
        var senderName : String = ""
        var created :Date!
        let clientArray = data["client"] as! [[String:Any]]
        let agentArray = data["agent"] as! [[String:Any]]
        
        for item in clientArray{
            senderID = item["sender_id"]! as! String
            content = item["content"]! as! String
            senderName = item["sender_name"]! as! String
            let createdTimeStamp = item["created"] as! TimeInterval
            created = NSDate(timeIntervalSinceReferenceDate: createdTimeStamp) as Date
            let message = Message(content: content,sentDate: created,senderId: senderID,senderName: senderName)
       //     insertNewMessage(message)
            messages.append(message)
        }
    
        for item in agentArray{
            senderID = item["sender_id"]! as! String
            content = item["content"]! as! String
            senderName = item["sender_name"]! as! String
            let createdTimeStamp = item["created"] as! TimeInterval
            created = NSDate(timeIntervalSinceReferenceDate: createdTimeStamp) as Date
            let message = Message(content: content,sentDate: created,senderId: senderID,senderName: senderName)
          // insertNewMessage(message)
            messages.append(message)
        }
        
       
        messages.sort()
        let shouldScrollToBottom = messagesCollectionView.isAtBottom
        
        messagesCollectionView.reloadData()
        
        if shouldScrollToBottom {
          DispatchQueue.main.async {
            self.messagesCollectionView.scrollToBottom(animated: true)
          }
        }
       

    }
  
//    @objc private func endChat() {
//      let ac = UIAlertController(title: nil, message: "Are you sure you want to end your chat?", preferredStyle: .alert)
//      ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
//        ac.addAction(UIAlertAction(title: "End Chat", style: .destructive, handler: { _ in
//            do {
//               // self.deleteChat()
//
//
//            } catch {
//                print("Error signing out: \(error.localizedDescription)")
//            }
//        }))
//        present(ac, animated: true, completion: nil)
//    }
    
    
   
    
  private func insertNewMessage(_ message: Message) {
//    guard !messages.contains(message) else {
//      return
//    }
//    messages.append(message)
    messages.sort()
   
    let isLatestMessage = messages.firstIndex(of: message) == (messages.count - 1)
    let shouldScrollToBottom = messagesCollectionView.isAtBottom && isLatestMessage
    
    messagesCollectionView.reloadData()
    
    if shouldScrollToBottom {
      DispatchQueue.main.async {
        self.messagesCollectionView.scrollToBottom(animated: true)
      }
    }
  }
  
}

// MARK: - MessagesDisplayDelegate

extension ChatViewController: MessagesDisplayDelegate {
  
  func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
    return isFromCurrentSender(message: message) ? .primary : .incomingMessage
  }
  
  func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> Bool {
    return false
  }
  
  func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
    let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
    return .bubbleTail(corner, .curved)
  }
  
}

// MARK: - MessagesLayoutDelegate

extension ChatViewController: MessagesLayoutDelegate {
  
  func avatarSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
    return .zero
  }
  
  func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
    return CGSize(width: 0, height: 8)
  }
  
  func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
    
    return 0
  }
  
}

// MARK: - MessagesDataSource

extension ChatViewController: MessagesDataSource {
  
  func currentSender() -> Sender {
    return Sender(id: ConstantStrings.agentID, displayName:ConstantStrings.agentName)
  }
  
  func numberOfMessages(in messagesCollectionView: MessagesCollectionView) -> Int {
    return messages.count
  }
  
  func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
    return messages[indexPath.section]
  }
  
  func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
    var name = message.sender.id
    if name.contains("agent"){
        name = "Support Agent"
    }
    return NSAttributedString(
      string: name,
      attributes: [
        .font: UIFont.preferredFont(forTextStyle: .caption1),
        .foregroundColor: UIColor(white: 0.3, alpha: 1)
      ]
    )
  }
  
}

// MARK: - MessageInputBarDelegate

extension ChatViewController: MessageInputBarDelegate {
  
  func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
    let message = Message(content: text)

    save(message)
    inputBar.inputTextView.text = ""
  }
  
}


