//
//  LoginViewController.swift
//  CustomerSupport
//
//  Created by mufaza on 11/06/2021.
//

import Foundation
import UIKit


class LoginViewController: UIViewController {
    
    @IBOutlet var actionButton: UIButton!
    @IBOutlet var fieldBackingView: UIView!
    @IBOutlet var displayNameField: UITextField!
    @IBOutlet var actionButtonBackingView: UIView!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fieldBackingView.smoothRoundCorners(to: 8)
        actionButtonBackingView.smoothRoundCorners(to: actionButtonBackingView.bounds.height / 2)
        
        displayNameField.tintColor = .primary
        displayNameField.addTarget(
            self,
            action: #selector(textFieldDidReturn),
            for: .primaryActionTriggered
        )
        
        registerForKeyboardNotifications()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        displayNameField.becomeFirstResponder()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.displayNameField.text = ""
        self.passwordField.text = ""
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    // MARK: - Actions
    
    @IBAction func actionButtonPressed() {
        signIn()
    }
    
    @objc  func textFieldDidReturn() {
        signIn()
    }
    
    @IBAction func forgorPasswordTapped(_ sender: Any) {
        self.showMissingNameAlert(message: "", title: "Please contact admin for support.")
        
    }
    // MARK: - Helpers
    
     func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow(_:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide(_:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil
        )
    }
    
     func signIn() {
        guard let name = displayNameField.text, !name.isEmpty else {
            showMissingNameAlert(message: "Please enter required fields", title: "Display Name and Password Required")
            return
        }
        
        guard let password = passwordField.text, !password.isEmpty else {
            showMissingNameAlert(message: "Please enter required fields", title: "Display Name and Password Required")
            return
        }
        
        displayNameField.resignFirstResponder()
        
        //AppSettings.displayName = name
        //call login API
        self.loginAPI()
        
    }
    
     func showMissingNameAlert(message:String,title:String) {
        //    let ac = UIAlertController(title: "Display Name and Password Required", message: "Please enter required fields correctly.", preferredStyle: .alert)
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Okay", style: .default, handler: { _ in
            DispatchQueue.main.async {
                self.displayNameField.becomeFirstResponder()
            }
        }))
        present(ac, animated: true, completion: nil)
    }
    
    //MARK: -API
    
    func loginAPI(){


        var dict : Dictionary <String, String> = [:]
        dict  = ["email" :displayNameField.text! as String,"password": passwordField.text! as String,"device_token":NetworkHelper.sharedInstance.DEVICE_TOKEN as String, "platform_id":"ios" as String]
        
        let data :Dictionary<String,Any> = ["data": dict as Any]
        print(data)
        
        NetworkHelper.sharedInstance.postChatRequest(serviceName:ApiUrl.LOGIN, sendData: data as [String : AnyObject], success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async { [self] in
                    // self.showAlert("Success")
                    let status = data["status"] as! Bool
                    if status{
                        print("successfully logged in")
                        UserDefaults.standard.set(displayNameField.text!, forKey: "userName")
                        UserDefaults.standard.set(true, forKey: "isLoggedIn")
                        UserDefaults.standard.set(data["data"]!["agent_id"] as! String,forKey: "agentID")
                        UserDefaults.standard.set(data["data"]!["base_url"] as! String,forKey: "baseURL")
                        UserDefaults.standard.set(data["data"]!["app_id"] as! String,forKey: "app_id")
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let destinationVC = storyboard.instantiateViewController(withIdentifier: "chatList")
                            self.navigationController?.pushViewController(destinationVC, animated: true)
                    }else{
                        showMissingNameAlert(message: "Enter correct UserName and Password to continue", title: "Invalid UserName or Password")
                    }
 
                }
                
            }else {
                DispatchQueue.main.async {
                    self.showMissingNameAlert(message: "Try again later", title: "Something went wrong")
                }
            }
            
        }, failure: { (data) in
            //print(data)
            self.showMissingNameAlert(message: "Try again later", title: "Something went wrong")
            
        })
        
    }
    
    // MARK: - Notifications
    
    @objc  func keyboardWillShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        guard let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height else {
            return
        }
        guard let keyboardAnimationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue else {
            return
        }
        guard let keyboardAnimationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue else {
            return
        }
        
        let options = UIView.AnimationOptions(rawValue: keyboardAnimationCurve << 16)
        bottomConstraint.constant = keyboardHeight + 20
        
        UIView.animate(withDuration: keyboardAnimationDuration, delay: 0, options: options, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc  func keyboardWillHide(_ notification: Notification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        guard let keyboardAnimationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue else {
            return
        }
        guard let keyboardAnimationCurve = (userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue else {
            return
        }
        
        let options = UIView.AnimationOptions(rawValue: keyboardAnimationCurve << 16)
        bottomConstraint.constant = 20
        
        UIView.animate(withDuration: keyboardAnimationDuration, delay: 0, options: options, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
}

