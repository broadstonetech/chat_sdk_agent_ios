//
//  ChatListViewController.swift
//  CustomerSupport
//
//  Created by mufaza on 11/06/2021.
//

import Foundation
import UIKit
 
class ChatListViewController:UIViewController {
    
    var NOTIFICATION_INFO: [AnyHashable : Any]!
    
    let toolbarLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    let channelCellIdentifier = "channelCell"
    var currentChannelAlertController: UIAlertController?
    
    var nonFilterData = [Channel]()
    var channels = [Channel]()
    
    var data :NSArray = []
    var list  = [String:Any]()
    
    
    
    lazy var refresher: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .gray
        refreshControl.addTarget(self, action: #selector(reloadData), for: .valueChanged)
        
        return refreshControl
    }()
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Support"
        self.navigationItem.hidesBackButton = true
        self.addDoneButtonOnKeyboard()
        self.searchBar.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(ChatListViewController.chatUpdatedNotificationReceived(notification:)), name: Notification.Name("Chatupdated"), object: nil)
        
        // clearsSelectionOnViewWillAppear = true
        //tableView.delegate = self
        tableView.refreshControl = refresher
        tableView.addSubview(refresher)
        NetworkHelper.sharedInstance.setUserData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isToolbarHidden = false
        
        
        
        
        var items = [UIBarButtonItem]()
        
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil) )
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil) )
        let titleButton = UIBarButtonItem(title: UserDefaults.standard.value(forKey: "userName") as? String, style: .plain, target: nil, action: nil)
        titleButton.isEnabled = false
        //    items.append(UIBarButtonItem(title: UserDefaults.standard.value(forKey: "userName") as? String, style: .plain, target: nil, action: nil))
        items.append(titleButton)
        
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil) )
        items.append(UIBarButtonItem(title: "Sign Out", style: .plain, target: self, action: #selector(signOut)))
        
        items[0].isEnabled = false
        
        self.toolbarItems = items
        toolbarItems = items
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // self.removeChannelFromTable()
        self.getChatList()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isToolbarHidden = true
    }
    
    //MARK: -API CALL
    func getChatList(){
        var dict : Dictionary <String, String> = [:]
        dict  = ["reciever_id":UserDefaults.standard.value(forKey: "agentID") as! String? ?? "","app_id":ConstantStrings.appID ]
        
        let data :Dictionary<String,Any> = ["data": dict as Any]
        print(data)
        
        NetworkHelper.sharedInstance.postChatRequest(serviceName:ApiUrl.GET_ACTIVE_CHATS, sendData: data as [String : AnyObject], success: { (data) in
            print(data)
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            let status = data["status"] as! Bool
            
            if ((statusCode?.range(of: "200")) != nil){
                DispatchQueue.main.async { [self] in
                    // self.showAlert("Success")
                    if status{
                        self.data = data["data"]!["refined_on_going_chat"] as! NSArray
                        self.parseData()
                    }else{
                        print("Error,something went wrong")
                    }
                    
                    
                }
                
            }
            
        }, failure: { (data) in
            print(data)
            
        })
    }
    
    func signOutApi(){
        
        var dict : Dictionary <String, String> = [:]
        dict  = ["agent_id":UserDefaults.standard.value(forKey: "agentID") as! String? ?? "","device_token":NetworkHelper.sharedInstance.DEVICE_TOKEN,"platform_id":"ios"]
        
        let data :Dictionary<String,Any> = ["data": dict as Any]
        print(data)
        
        NetworkHelper.sharedInstance.postChatRequest(serviceName:ApiUrl.SIGNOUT, sendData: data as [String : AnyObject], success: { (data) in
            let defaults = UserDefaults.standard
            let statusCode = defaults.string(forKey: "statusCode")
            if ((statusCode?.range(of: "200")) != nil){
                let status = data["status"] as! Bool
                DispatchQueue.main.async { [self] in
                    // self.showAlert("Success")
                    if status{
                        NetworkHelper.sharedInstance.clearUserData()
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let initialViewController = storyboard.instantiateViewController(withIdentifier: "login")
                        let mainViewController = UINavigationController(rootViewController: initialViewController)
                        let shared = UIApplication.shared.delegate as? AppDelegate
                        
                        shared?.window?.rootViewController = mainViewController
                        shared?.window?.makeKeyAndVisible()
                        
                        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
                    }else{
                        self.showAlert(withTitle: "Error", withMessage: "Something went wrong.Try again later")
                    }
                    
                }
                
            }
            
        }, failure: { (data) in
            print(data)
            
        })
        
    }
    
    // MARK: - Actions
    
    @objc func signOut() {
        let ac = UIAlertController(title: nil, message: "Are you sure you want to sign out?", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        ac.addAction(UIAlertAction(title: "Sign Out", style: .destructive, handler: { _ in
            
            self.signOutApi()
            
        }))
        present(ac, animated: true, completion: nil)
    }
    
    
    
    
    @objc  func textFieldDidChange(_ field: UITextField) {
        guard let ac = currentChannelAlertController else {
            return
        }
        
        ac.preferredAction?.isEnabled = field.hasText
    }
    @objc func reloadData() {
        
        let deadline = DispatchTime.now() + .milliseconds(700)
        DispatchQueue.main.asyncAfter(deadline: deadline) {
            self.getChatList()
            self.refresher.endRefreshing()
        }
    }
    
    // MARK: - Helpers
    
    @objc func chatUpdatedNotificationReceived(notification: Notification) {
        // Take Action on Notification
        // self.removeChannelFromTable()
        NOTIFICATION_INFO = notification.userInfo
        self.getChatList()
    }
    
    func showAlert(withTitle title: String, withMessage message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
        alert.addAction(ok)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    func parseData()
    {
        var firebaseID:String = ""
        var name :String = ""
        var status : Bool
        channels.removeAll()
        
        for item in data{
            list  = item as! [String : Any]
            firebaseID = list["firebase_id"] as! String
            name = list["creator_name"] as! String
            status = list["read_by_agent"] as! Bool
            let channel =  Channel(id: firebaseID, creator: name, status: status)
            //channels.append(channel)
            addChannelToTable(channel)
            
            
        }
        self.tableView.reloadData()
        
        if NOTIFICATION_INFO != nil {
            let fid = (NOTIFICATION_INFO["firebase_id"] as? String) ?? ""

            let channel = channels.first(where: {
                $0.id == fid
            })
            if channel != nil {
                let vc = ChatViewController(channel: channel!)
                navigationController?.pushViewController(vc, animated: true)
                NOTIFICATION_INFO.removeAll()
            }

        }
    }
    
    
    func addChannelToTable(_ channel: Channel) {
        guard !channels.contains(channel) else {
            return
        }
        
        channels.append(channel)
        nonFilterData.append(channel)
        nonFilterData.sort()
        
        guard let index = channels.index(of: channel) else {
            return
        }
    }
    
    //MARK: - Add Done on KeyBoard
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.searchBar.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        
        self.searchBar.resignFirstResponder()
    }
    
    
}

// MARK: - TableViewDelegate

extension ChatListViewController:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channels.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: channelCellIdentifier, for: indexPath)
        
        cell.accessoryType = .disclosureIndicator
        cell.textLabel?.text = channels[indexPath.row].creator
        
        let unreadImage : UIImage = UIImage(named: "message-icon2")!
        let imageView = UIImageView(image: unreadImage)
        
        imageView.frame = CGRect(x:cell.bounds.width - 50, y: ((cell.bounds.height/2) - 15), width: 20, height: 20)
        
        
        if channels[indexPath.row].status {
            //  cell.imageView?.image = unreadImage
            cell.imageView?.image = nil
            
            
        }else{
            // view.willRemoveSubview(imageView)
            // cell.imageView?.image = nil
            cell.imageView?.image = unreadImage
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel = channels[indexPath.row]
        //channelReference.document(channel.id!).updateData(["status":"Read"])
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = ChatViewController(channel: channel)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}
//MARK: - SEARCHBAR
extension ChatListViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        if searchBar.text != "" {
            self.channels.removeAll()
            channels = nonFilterData.filter{ $0.creator.lowercased().contains(searchBar.text!.lowercased()) }
            
            
            self.tableView.reloadData()
        }else{
            self.channels.removeAll()
            channels = self.nonFilterData
            self.tableView.reloadData()
        }
    }
}
