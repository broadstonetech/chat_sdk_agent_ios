//
//  NetworkHelper.swift
//  CustomerSupport
//
//  Created by mufaza on 12/06/2021.
//

import Foundation
import SystemConfiguration
import UIKit

struct ConstantStrings {
  static var internet_off = "Application is unable to reach the Internet. Please check your network connection and try again."

    static var appID = "parsl"
    static var agentID = ""
    static var agentName = ""
   // static var aapModeBaseURl = ""
    static var aapModeBaseURl = "https://chat.broadstone.tech/"
}

struct ApiUrl {
    
    static let LOGIN = "https://chat.broadstone.tech/agent/login"
    static let SIGNOUT = "https://chat.broadstone.tech/agent/logout"
    
    
    static let GET_ACTIVE_CHATS = "get/active/chat"
    static let SAVE_CHAT = "save/chat"
    static let START_CHAT =  "start/chat"
    static let CONTINUE_CHAT =  "continue/chat"
    static let PUSH_AGENT_MSG = "push/user/msg"
    static let GET_USER_MSG = "get/user/msgs"
    
}

class NetworkHelper: NSObject {
  
    static let sharedInstance = NetworkHelper()
    var DEVICE_TOKEN: String = ""
  

    var error401 = false
    var error403 = false
    var isConnectedDevicesSuccess = false
    
    var shoulUpdateChat = false
    
    
    func clearUserData(){
        UserDefaults.standard.removeObject(forKey: "userName")
        UserDefaults.standard.removeObject(forKey: "isLoggedIn")
        UserDefaults.standard.removeObject(forKey: "agentID")
        UserDefaults.standard.removeObject(forKey: "baseURL")
        
    }
    
    func setUserData(){
        ConstantStrings.agentName =  UserDefaults.standard.value(forKey: "userName") as! String? ?? ""
        ConstantStrings.agentID =  UserDefaults.standard.value(forKey: "agentID") as! String? ?? ""
        ConstantStrings.aapModeBaseURl = UserDefaults.standard.value(forKey: "baseURL") as! String? ?? ""
        ConstantStrings.appID = UserDefaults.standard.value(forKey: "app_id") as! String? ?? ""
    }
  
  
  //MARK: - PostRequest for chat module
  func postChatRequest(serviceName: String,sendData : [String : AnyObject],success:@escaping ( _ data : [String : AnyObject])->Void, failure:@escaping ( _ data:[String : AnyObject])->Void) {

      
      //        if(NetworkHelper.sharedInstance.isDmoatOnline){
      
    //  CozyLoadingActivity.show("Please Wait..", disableUI: true)
    var serviceUrl :String = ""
    
    if serviceName == ApiUrl.LOGIN || serviceName == ApiUrl.SIGNOUT{
        serviceUrl = serviceName
    }else{
        serviceUrl = ConstantStrings.aapModeBaseURl + serviceName
    }
    
     
     // let serviceUrl =  serviceName
      print("url == \(serviceUrl)")
      let url = NSURL(string: serviceUrl)
      
      //create the session object
      let session = URLSession.shared
      
      //now create the NSMutableRequest object using the url object
      let request = NSMutableURLRequest(url: url! as URL)
      request.httpMethod = "POST" //set http method as POST
//        request.timeoutInterval = 20
      
      do {
          
          let data = try JSONSerialization.data(withJSONObject: sendData ,options: .prettyPrinted)
          let dataString = String(data: data, encoding: .utf8)!
          print("data string is",dataString)
          
          request.httpBody = try JSONSerialization.data(withJSONObject: sendData, options: .prettyPrinted)
          
      } catch let error {
          print(error.localizedDescription)
      }
      
      //HTTP Headers
      request.addValue("application/json", forHTTPHeaderField: "Content-Type")
      request.addValue("application/json", forHTTPHeaderField: "Accept")
      
      //create dataTask using the session object to send data to the server
      
      let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
          
          print(" response is \(response)")
          
          //copied
          if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
              // check for http errors
              print("statusCode should be 200, but is \(httpStatus.statusCode)")
              
              UserDefaults.standard.set(httpStatus.statusCode, forKey: "statusCode")
              UserDefaults.standard.synchronize()
              
              if(httpStatus.statusCode == 401)
              {
                  NetworkHelper.sharedInstance.error401 = true
              }
              else if(httpStatus.statusCode == 403)
              {
                  NetworkHelper.sharedInstance.error403 = true
              }
              
          }
          let httpStats = response as? HTTPURLResponse
          UserDefaults.standard.set(httpStats?.statusCode, forKey: "statusCode")
          UserDefaults.standard.synchronize()
          //end copied
          
          guard error == nil else {
              print("this is error")
              DispatchQueue.main.async {
                 // CozyLoadingActivity.hide()
                  self.showFailureAlert(ConstantStrings.internet_off)
                  
              }
              return
          }
          guard let data = data else {
              return
          }
          
          do {
              //create json object from data
              
              if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject] {
                  print("json is \(json)")
                  NetworkHelper.sharedInstance.isConnectedDevicesSuccess = true
                  success(json)
                  //handle json...
                 // CozyLoadingActivity.hide()
              }
              
             // CozyLoadingActivity.hide(true, animated: false)
          } catch let error {
             // CozyLoadingActivity.hide()
              print(error.localizedDescription)
              var errors = [String : AnyObject]()
              errors["Error"] = "Server Error" as AnyObject
              failure(errors)
          }
          
      })
      
      task.resume()
      
  }
  
  
  func showFailureAlert(_ message:String)
  {
      let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertController.Style.alert)
      alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action) in
          NotificationCenter.default.post(name: Notification.Name("Chatupdated"), object: nil)
      }))
      UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
      
  }
  
}



