//
//  main.swift
//  CustomerSupport
//
//  Created by Naqash Ali on 17/03/2022.
//

import Foundation
import UIKit

UIApplicationMain(
CommandLine.argc, UnsafeMutableRawPointer(CommandLine.unsafeArgv)
    .bindMemory( to: UnsafeMutablePointer<Int8>.self,
        capacity: Int(CommandLine.argc)), nil, NSStringFromClass(AppDelegate.self))
