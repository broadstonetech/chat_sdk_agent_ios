//
//  AppDelegate.swift
//  CustomerSupport
//
//  Created by mufaza on 12/06/2021.
//

import UIKit
import Firebase

//@main
//UIApplicationMain(
//CommandLine.argc, CommandLine.unsafeArgv,
//NSStringFromClass(CustomApplication.self),
//NSStringFromClass(AppDelegate.self)
//)
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {


    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    var rootViewController : UIViewController?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
          UNUserNotificationCenter.current().delegate = self

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        
        
        
        let isLoggedIn = UserDefaults.standard.value(forKey: "isLoggedIn") as! Bool? ?? false
        print(isLoggedIn)
        
        if isLoggedIn{
           // rootViewController = ChatListViewController()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "chatList")
            let mainViewController = UINavigationController(rootViewController: initialViewController)
            self.window?.rootViewController = mainViewController
            self.window?.makeKeyAndVisible()
        }

        
        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("Firebase registration token: \(fcmToken)")
          
        let dataDict:[String: String] = ["token": fcmToken!]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        NetworkHelper.sharedInstance.DEVICE_TOKEN = fcmToken!
         // TODO: If necessary send token to application server.
         // Note: This callback is fired at each app startup and whenever a new token is generated.
     }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        print("82")

        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        NotificationCenter.default.post(name: Notification.Name("Chatupdated"), object: nil, userInfo: userInfo)
        //coordinateToSomeVC(userInfo: userInfo)
 
    }


    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
      withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
      let userInfo = notification.request.content.userInfo

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // ...

      // Print full message.
        print("97-----")
      print(userInfo)
      
        
        let state = UIApplication.shared.applicationState
        if state == .background || state == .inactive {
            // background
        } else if state == .active {
            NotificationCenter.default.post(name: Notification.Name("Chatupdated"), object: nil)
        }

        
      // Change this to your preferred presentation option
      completionHandler([[.alert, .sound]])
    }
    
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
 
    
    private func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("device token is",deviceToken)
      Messaging.messaging().apnsToken = deviceToken
        
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingDelegate) {

        print("ReceivedDataMessage 71: \(remoteMessage.description)")
        
        //NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
        
        let state = UIApplication.shared.applicationState
        if state == .background || state == .inactive {
            // background
        } else if state == .active {
            NotificationCenter.default.post(name: Notification.Name("Chatupdated"), object: nil)
        }

    }
    

    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print("ReceivedDataMessage 91: \(userInfo)")
        
        let state = UIApplication.shared.applicationState
        if state == .background || state == .inactive {
            
        } else if state == .active {
            
        }
        NotificationCenter.default.post(name: Notification.Name("Chatupdated"), object: nil, userInfo: userInfo)
        
    }


    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
       Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
      if let messageID = userInfo[gcmMessageIDKey] {
        print("Message ID: \(messageID)")
      }
        print("116")

      // Print full message.
      print(userInfo)
        NotificationCenter.default.post(name: Notification.Name("Chatupdated"), object: nil, userInfo: userInfo)
    }
    
}

