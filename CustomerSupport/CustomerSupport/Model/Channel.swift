//
//  Channel.swift
//  CustomerSupport
//
//  Created by mufaza on 11/06/2021.
//

struct Channel {

  let id: String
  let creator:String
  let status:Bool

    init(id: String,creator:String,status:Bool) {
    self.id = id
    self.creator = creator
    self.status = status
   
  }
    
    init(id:String)
    {
        self.id  = id
        self.creator = ""
        self.status = true
    }

}



extension Channel: Comparable {

  static func == (lhs: Channel, rhs: Channel) -> Bool {
    return lhs.id == rhs.id
  }

  static func < (lhs: Channel, rhs: Channel) -> Bool {
    return lhs.creator < rhs.creator
  }

}
