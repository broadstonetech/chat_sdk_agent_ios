//
//  DatabaseRepresentation.swift
//  CustomerSupport
//
//  Created by mufaza on 11/06/2021.
//

import Foundation

protocol DatabaseRepresentation {
  var representation: [String: Any] { get }
}
