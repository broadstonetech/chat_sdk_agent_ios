
//
//  Message.swift
//  CustomerSupport
//
//  Created by mufaza on 11/06/2021.
//

import MessageKit

struct Message: MessageType {
  
  let id: String?
  let content: String
  let sentDate: Date
  let sender: Sender
  
  var data: MessageData {
    if let image = image {
      return .photo(image)
    } else {
      return .text(content)
    }
  }
  
  var messageId: String {
    return id ?? UUID().uuidString
  }
  
  var image: UIImage? = nil
  var downloadURL: URL? = nil
  
  init(content: String) {
    sender = Sender(id: ConstantStrings.agentID, displayName:ConstantStrings.agentName)
    self.content = content
    sentDate = Date()
    id = nil
  }
  
  init( image: UIImage) {
    sender = Sender(id: ConstantStrings.agentID, displayName:ConstantStrings.agentName)
    self.image = image
    content = ""
    sentDate = Date()
    id = nil
  }
    
    init(content:String,sentDate:Date,senderId:String,senderName:String){
        sender = Sender(id: senderId, displayName:senderName)
        self.content = content
        self.sentDate = sentDate
        id = nil
        
    }
}

extension Message: DatabaseRepresentation {
  
  var representation: [String : Any] {
    var rep: [String : Any] = [
      "created": sentDate,
      "senderID": sender.id,
      "senderName": sender.displayName
    ]
    
    if let url = downloadURL {
      rep["url"] = url.absoluteString
    } else {
      rep["content"] = content
    }
    
    return rep
  }
  
}

extension Message: Comparable {
  
  static func == (lhs: Message, rhs: Message) -> Bool {
    return lhs.id == rhs.id
  }
  
  static func < (lhs: Message, rhs: Message) -> Bool {
    return lhs.sentDate < rhs.sentDate
  }
  
}

